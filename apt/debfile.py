import inary
import inary.package
import inary.db.installdb
class NoDebArchiveException(IOError):
    """Exception which is raised if a file is no Debian archive."""


class DebPackage(object):
    """A Debian Package (.deb file)."""
    def __init__(self, filename=None, cache=None):
        ipkg=inary.package.Package(filename).metadata.package
        idb=inary.db.installdb.InstallDB()
        self._need_pkgs = []
        self._check_was_run = False
        self._failure_string = ""
        self._multiarch = None  # type: Optional[str]
        self.missing_deps=[]
        self._cache = cache
        self._debfile = None
        self.pkgname = ipkg.name
        self.filename = filename
        runtimedep=""
        for i in ipkg.runtimeDependencies():
            runtimedep=runtimedep+str(i.name())+", "
            if not idb.has_package(str(i.name())):
                print(i.name())
                self.missing_deps.append(str(i.name()))
        runtimedep=runtimedep.strip()[:-1]
        self._sections = dict({
            "Version":ipkg.version,
            "Description":ipkg.summary["en"],
            "Maintainer":"Unknown",
            "Priority":ipkg.partOf,
            "Section":ipkg.isA[0],
            "Installed-Size":str(ipkg.installedSize/1024),
            "Architecture":ipkg.architecture,
            "Depends":runtimedep
            })
        
    def compare_to_version_in_cache(self):
        return True
    def check(self):
        return True
