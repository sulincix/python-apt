import apt.package
import inary.db.packagedb
class Cache(object):
    def __init__(self, progress=None, rootdir=None, memonly=False):
        pdb=inary.db.packagedb.PackageDB()
        # type: (Optional[OpProgress], Optional[str], bool) -> None
        self._cache = None  # type: apt_pkg.Cache
        self._depcache = None  # type: apt_pkg.DepCache
        self._records = None  # type: apt_pkg.PackageRecords # noqa
        self._list = None  # type: apt_pkg.SourceList
        self._callbacks = {}  # type: Dict[str, List[Union[Callable[..., None],str]]] # noqa
        self._set = set()
        for i in pdb.list_packages():
            self._set.add(i)
        self._fullnameset = set()
        self._callbacks2 = {}  # type: Dict[str, List[Tuple[Callable[..., Any], Tuple[Any, ...], Dict[Any,Any]]]] # noqa
        self._weakref = None  # type: weakref.WeakValueDictionary[str, apt.Package] # noqa
        self._weakversions = None  # type: weakref.WeakSet[Version] # noqa
        self._changes_count = -1
        self._sorted_set = None  # type: Optional[List[str]]
        self.broken_count = 0 # type: integer 

    def __getitem__(self, key):
        pkg=apt.package.Package(self._cache,key)
        return pkg
        
    def installed(self):
        pass
        
    def upgrade(self, dist_upgrade=False):
        pass
        
    def get_changes(self):
        return []
        
    def keys(self):
        return self._set
